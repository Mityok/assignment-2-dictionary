extern string_equals  ; no need to import all library (I use one function)
%macro push_macro 0
push r12
push r13
%endmacro

%macro pop_macro 0
pop r13
pop r12
%endmacro

%define size_of_word 8

section .text

global find_word

find_word:

	push_macro
	mov r12, rdi    ; -- point on null string
	mov r13, rsi    ; -- point on start dict


.loop:
	test r13, r13
	jz .end_of_dict

	mov rdi, r12
	add rsi, size_of_word
	call string_equals   ; chech that we found right word
	test rax, rax
	jnz .end             ; if we found word, then go end

	mov rsi, [r13]
	mov r13, rsi
	jmp .loop


.end:                    ; ret when we find word
	mov rax, r13
	pop_macro
	ret

 
.end_of_dict:            ; ret when we not found word
	xor rax, rax
	pop_macro
	ret
