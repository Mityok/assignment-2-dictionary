import subprocess
f = open('file1.txt')
data = []
for s in f:
	s = s.split('\t')
	m = {"input": s[0], "output": s[1][:-1]}
	data.append(m)
k = 0
myProgram = './program'
keyInput = "input"
keyOutput = "output"

flag = True
for a in data:
	process = subprocess.Popen([myProgram], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)

	input = a[keyInput]
	expected_output = a[keyOutput]

	process.stdin.write(input)

	result = process.communicate()
	stdout = result[0]
	stderr = result[1]
	
	print(stdout, stderr, expected_output)
	if (stdout == expected_output) or (stderr == expected_output):
		print("The test was successful ")
	else:
		flag = False
		print("The test failed your output " + stdout + " expected output " + expected_output)

print("................")
if flag == True:
	print("All tests passed!")
