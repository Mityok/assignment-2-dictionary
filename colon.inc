%define next_elem 0     ; last element will have zero

%macro colon 2
%ifid %2
%2: 
dq next_elem
%endif
%ifstr %1
db %1, 0
%endif
%define next_elem %2    ; other elements pointing to the next element
%endmacro
