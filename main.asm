%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define size_of_buffer 257
%define size_of_big_word_message 19
%define size_of_word_not_found_message 18
%define SYSCALL_WRITE 1
%define STDERR 2

section .data
	big_word_message: db "The word is too big", 0
	word_not_found_message: db "The word not found", 0

section .bss
	buffer: resb size_of_buffer

section .text

global _start

_start:
	mov rdi, buffer
	mov rsi, size_of_buffer
	call read_word        ; reading the word we want to find
	
	test rax, rax
	jz .big_word
	mov rdi, buffer
	mov rsi, next_elem    ; from colon.inc
	call find_word        ; find our word
	cmp rax, 0
	jz .word_not_found 
	
	mov rdi, rax          ; find our value
	add rdi, 8
	call string_length
	add rdi, rax
	inc rdi

	call print_string     ; print our word     WORD ---> STDOUT
	call exit
	
	
.big_word:           ; The word has more than 256 symbols
	mov rdi, big_word_message
	call _print_string_in_stderr
	call exit

.word_not_found:     ; dictionary hasn't our word
	mov rdi, word_not_found_message
	call _print_string_in_stderr
	call exit


_print_string_in_stderr:
    push rdi
    xor rax, rax
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDERR
    syscall     ;message --->  STDERR
    ret


