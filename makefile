
.PHONY: clean program test

%.o: %.asm
	nasm -g -felf64 -o $@ $<


program: main.o lib.o dict.o
	ld -o program main.o lib.o dict.o

clean:
	rm -f *.o program

test:
	python test.py
